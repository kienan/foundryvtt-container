# Building

1. Download foundry version to the local directory, eg. `FoundryVTT-10.285.zip`
2. Build the container

```
podman build --build-arg FOUNDRY_VERSION=10.285 -t foundry:10.285 .
```

# Running with podman-compose

```
podman-compose up -d
```
